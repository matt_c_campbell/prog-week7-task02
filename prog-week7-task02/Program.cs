﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

    Author:         Matt. Campbell.
    Date Created:   12th September, 2016.

    Task 02:        Print the WriteLines() method from Task 01 another 3 times.

*/

namespace prog_week7_task01
{

    class Program
    {

/*—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

        static void Main(string[] args)
        {
            WriteLines();
            WriteLines();
            WriteLines();
            WriteLines();

            Exit();
        }

/*—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

        static void WriteLines()
        {
            Console.WriteLine("This is a method");
            Console.WriteLine("This second line is now printed to the screen\n");
        }

/*—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

        static void Exit()
        {
            Console.WriteLine("-----------------------------------------------------------------------------------------------------------------------");

            Console.WriteLine("Press [Enter] to exit program . . .");
            Console.ReadLine();
        }

/*—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

    }

}